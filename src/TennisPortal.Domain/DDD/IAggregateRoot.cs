﻿using System.Collections.Generic;
using TennisPortal.Cqrs;

namespace TennisPortal.Domain.DDD
{
    public interface IAggregateRoot
    {
        IReadOnlyList<IDomainEvent> RaisedEvents { get; }

        void ClearEvents();
    }
}

﻿using System.Threading.Tasks;

namespace TennisPortal.Domain.DDD
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}

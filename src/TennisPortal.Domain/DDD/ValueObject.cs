﻿using System.Collections.Generic;
using System.Linq;

namespace TennisPortal.Domain.DDD
{
    public abstract class ValueObject<T> where T : ValueObject<T>
    {
        protected abstract IEnumerable<object> GetAttributesForEqualityCheck();

        public override bool Equals(object? other)
        {
            if (!(other is T typedOther))
            {
                return false;
            }

            return GetAttributesForEqualityCheck().SequenceEqual(
                typedOther.GetAttributesForEqualityCheck());
        }

        public static bool operator ==(ValueObject<T> left, ValueObject<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ValueObject<T> left, ValueObject<T> right)
        {
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            var hash = 17;

            foreach (var obj in GetAttributesForEqualityCheck())
            {
                hash *= 31;
                hash += obj?.GetHashCode() ?? 0;
            }

            return hash;
        }
    }
}

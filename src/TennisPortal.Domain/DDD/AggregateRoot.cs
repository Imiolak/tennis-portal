﻿using System.Collections.Generic;
using System.Linq;
using TennisPortal.Cqrs;

namespace TennisPortal.Domain.DDD
{
    public abstract class AggregateRoot<TId> : Entity<TId>, IAggregateRoot
    {
        private IList<IDomainEvent> _raisedEvents = new List<IDomainEvent>();

        public IReadOnlyList<IDomainEvent> RaisedEvents => _raisedEvents.ToList();

        protected AggregateRoot(TId id)
            : base(id)
        {
        }

        protected AggregateRoot()
            : base()
        {
        }

        protected void RaiseEvent(IDomainEvent @event)
        {
            _raisedEvents.Add(@event);
        }

        public void ClearEvents()
        {
            _raisedEvents.Clear();
        }
    }
}

﻿using System.Threading.Tasks;

namespace TennisPortal.Domain.Identity.Membership.Repositories
{
    public interface IPasswordHashService
    {
        Task<string> HashPassword(Password password);
        Task<bool> VerifyPassword(string hashedPassword, string password);
    }
}

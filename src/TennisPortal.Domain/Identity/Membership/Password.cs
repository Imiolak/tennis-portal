﻿using FluentResults;
using System;
using System.Collections.Generic;
using System.Linq;
using TennisPortal.Domain.DDD;
using TennisPortal.Domain.Shared.Errors;

namespace TennisPortal.Domain.Identity.Membership
{
    public sealed class Password : ValueObject<Password>
    {
        private Password(string password)
        {
            Value = password;
        }

        public string Value { get; }

        public static Result<Password> Create(string password, string passwordConfirmation)
        {
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (passwordConfirmation == null)
            {
                throw new ArgumentNullException(nameof(passwordConfirmation));
            }

            if (password.Length < 8)
            {
                return Result.Fail(FormatErrors.ValueTooShort());
            }

            if (password.Length > 50)
            {
                return Result.Fail(FormatErrors.ValueTooLong());
            }

            if (!ValidatePasswordFormat(password))
            {
                return Result.Fail(FormatErrors.InvalidFormat());
            }

            if (password != passwordConfirmation)
            {
                return Result.Fail(MembershipErrors.PasswordsDontMatch());
            }

            return Result.Ok(
                new Password(password));
        }

        protected override IEnumerable<object> GetAttributesForEqualityCheck()
        {
            yield return Value;
        }

        private static bool ValidatePasswordFormat(string password)
        {
            if (!password.Any(c => char.IsUpper(c)))
            {
                return false;
            }

            if (!password.Any(c => char.IsLower(c)))
            {
                return false;
            }

            if (!password.Any(c => char.IsDigit(c)))
            {
                return false;
            }

            return true;
        }
    }
}

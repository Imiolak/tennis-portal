﻿using FluentResults;

namespace TennisPortal.Domain.Identity.Membership
{
    public static class MembershipErrors
    {
        public static Error UsernameNotUnique() => new Error("username.not.unique");
        public static Error EmailNotUnique() => new Error("email.not.unique");
        public static Error PasswordsDontMatch() => new Error("passwords.dont.match");
    }
}

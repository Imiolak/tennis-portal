﻿using System.Threading.Tasks;

namespace TennisPortal.Domain.Identity.Membership.Services.Repositories
{
    public interface IUserRepository
    {
        Task Add(IdentityUser newUser);
        Task<bool> ContainsEmail(EmailAddress email);
        Task<bool> ContainsUsername(Username username);
    }
}

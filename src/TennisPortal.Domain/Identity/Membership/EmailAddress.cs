﻿using FluentResults;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TennisPortal.Domain.DDD;
using TennisPortal.Domain.Shared.Errors;

namespace TennisPortal.Domain.Identity.Membership
{
    public sealed class EmailAddress : ValueObject<EmailAddress>
    {
        private static readonly Regex EmailRegex = new Regex(
            @"^[a-zA-Z0-9_\+-]+(\.[a-zA-Z0-9_\+-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,})$",
            RegexOptions.Compiled);

        private EmailAddress(string value)
        {
            Value = value;
        }

        public string Value { get; }

        public static Result<EmailAddress> Create(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            email = email.Trim();

            if (!EmailRegex.IsMatch(email))
            {
                return Result.Fail(FormatErrors.InvalidFormat());
            }

            return Result.Ok(
                new EmailAddress(email));
        }

        public override string ToString() => Value;

        protected override IEnumerable<object> GetAttributesForEqualityCheck()
        {
            yield return Value.ToLower();
        }
    }
}

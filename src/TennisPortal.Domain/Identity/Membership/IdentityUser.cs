﻿using System;
using TennisPortal.Domain.DDD;

namespace TennisPortal.Domain.Identity.Membership
{
    public class IdentityUser : AggregateRoot<UserId>
    {
        public IdentityUser(
            Username username,
            EmailAddress email,
            string hashedPassword)
            : base(new UserId(Guid.NewGuid()))
        {
            Username = username ?? throw new ArgumentNullException(nameof(username));
            Email = email ?? throw new ArgumentNullException(nameof(email));
            HashedPassword = hashedPassword ?? throw new ArgumentNullException(nameof(hashedPassword));

            RaiseEvent(new UserCreatedEvent(
                Id,
                Username,
                Email));
        }

        protected IdentityUser()
        {
        }

        public Username Username { get; }
        public EmailAddress Email { get; }
        public string HashedPassword { get; }
    }
}

﻿using FluentResults;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TennisPortal.Domain.DDD;
using TennisPortal.Domain.Shared.Errors;

namespace TennisPortal.Domain.Identity.Membership
{
    public sealed class Username : ValueObject<Username>
    {
        private const int MinimumUsernameLength = 3;
        private const int MaximumUsernameLength = 20;

        private static readonly Regex UsernameFormat = new Regex(
            "^[a-zA-Z0-9]+$",
            RegexOptions.Compiled);

        private Username(string value)
        {
            Value = value;
        }

        public string Value { get; }

        public static Result<Username> Create(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (username.Length < MinimumUsernameLength)
            {
                return Result.Fail(FormatErrors.ValueTooShort());
            }

            if (username.Length > MaximumUsernameLength)
            {
                return Result.Fail(FormatErrors.ValueTooLong());
            }

            if (!UsernameFormat.IsMatch(username))
            {
                return Result.Fail(FormatErrors.InvalidFormat());
            }

            return Result.Ok(new Username(username));
        }

        public override string ToString() => Value;

        protected override IEnumerable<object> GetAttributesForEqualityCheck()
        {
            yield return Value.ToLower();
        }
    }
}

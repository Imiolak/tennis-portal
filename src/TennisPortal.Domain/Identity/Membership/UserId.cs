﻿using System;
using System.Collections.Generic;
using TennisPortal.Domain.DDD;

namespace TennisPortal.Domain.Identity.Membership
{
    public class UserId : ValueObject<UserId>
    {
        public UserId(Guid value)
        {
            Value = value;
        }

        public Guid Value { get; }

        public override string ToString() => Value.ToString();

        protected override IEnumerable<object> GetAttributesForEqualityCheck()
        {
            yield return Value;
        }
    }
}

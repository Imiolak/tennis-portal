﻿using TennisPortal.Cqrs;

namespace TennisPortal.Domain.Identity.Membership
{
    public class UserCreatedEvent : IDomainEvent
    {
        public UserCreatedEvent(
            UserId userId,
            Username username,
            EmailAddress email)
        {
            UserId = userId;
            Username = username;
            Email = email;
        }

        public UserId UserId { get; }
        public Username Username { get; }
        public EmailAddress Email { get; }
    }
}

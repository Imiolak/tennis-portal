﻿using FluentResults;
using System.Linq;

namespace TennisPortal.Domain.Shared.Errors
{
    public static class ResultBaseExtensions
    {
        public static bool HasError(
            this ResultBase result,
            Error error)
        {
            var errorTypes = result.Errors.Select(e => e.Message.ToLower());
            return errorTypes.Contains(error.Message.ToLower());
        }
    }
}

﻿using FluentResults;

namespace TennisPortal.Domain.Shared.Errors
{
    public static class FormatErrors
    {
        public static Error ValueTooShort() => new Error("value.too.short");
        public static Error ValueTooLong() => new Error("value.too.long");
        public static Error InvalidFormat() => new Error("invalid.format");
    }
}

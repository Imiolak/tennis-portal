﻿using FluentResults;
using System;
using System.Threading;
using System.Threading.Tasks;
using TennisPortal.Cqrs;
using TennisPortal.Domain.DDD;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Identity.Membership.Repositories;
using TennisPortal.Domain.Identity.Membership.Services.Repositories;

namespace TennisPortal.Application.Identity.Membership
{
    public class CreateUserCommandExecutor : ICommandExecutor<CreateUserCommand, Guid>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEventDispatcher _eventPublisher;
        private readonly IUserRepository _users;
        private readonly IPasswordHashService _passwordHashService;

        public CreateUserCommandExecutor(
            IUnitOfWork unitOfWork,
            IEventDispatcher eventPublisher,
            IUserRepository users,
            IPasswordHashService passwordHashService)
        {
            _unitOfWork = unitOfWork;
            _eventPublisher = eventPublisher;
            _users = users;
            _passwordHashService = passwordHashService;
        }

        public async Task<Result<Guid>> Execute(
            CreateUserCommand command,
            CancellationToken cancellationToken)
        {
            var usernameOrError = Username.Create(command.Username);
            var emailOrError = EmailAddress.Create(command.Email);
            var passwordOrError = Password.Create(
                command.Password,
                command.PasswordConfirmation);

            var validation = Result.Merge(
                usernameOrError,
                emailOrError,
                passwordOrError);

            if (validation.IsFailed)
            {
                return validation.ToResult<Guid>();
            }

            var username = usernameOrError.Value;
            var email = emailOrError.Value;
            var password = passwordOrError.Value;

            if (await _users.ContainsEmail(email))
            {
                return Result.Fail(MembershipErrors.EmailNotUnique());
            }

            if (await _users.ContainsUsername(username))
            {
                return Result.Fail(MembershipErrors.UsernameNotUnique());
            }

            var hashedPassword = await _passwordHashService.HashPassword(password);

            var newUser = new IdentityUser(
                usernameOrError.Value,
                emailOrError.Value,
                hashedPassword);

            await _users.Add(newUser);
            await _unitOfWork.Commit();

            return Result.Ok(newUser.Id.Value);
        }
    }
}

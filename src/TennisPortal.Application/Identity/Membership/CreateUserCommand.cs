﻿using System;
using TennisPortal.Cqrs;

namespace TennisPortal.Application.Identity.Membership
{
    public class CreateUserCommand : ICommand<Guid>
    {
        public CreateUserCommand(
            string username,
            string email,
            string password,
            string passwordConfirmation)
        {
            Username = username;
            Email = email;
            Password = password;
            PasswordConfirmation = passwordConfirmation;
        }

        public string Username { get; }
        public string Email { get; }
        public string Password { get; }
        public string PasswordConfirmation { get; }
    }
}

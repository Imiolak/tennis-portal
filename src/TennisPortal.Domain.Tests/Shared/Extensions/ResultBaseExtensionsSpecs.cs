﻿using FluentAssertions;
using FluentResults;
using TennisPortal.Domain.Shared.Errors;
using Xunit;

namespace TennisPortal.Domain.Tests.Shared.Extensions
{
    public class ResultBaseExtensionsSpecs
    {
        [Fact]
        public void Contains_error_with_the_same_message()
        {
            // Arrange
            const string message = "error.message";
            var result = Result.Fail(new Error(message));

            // Act
            var hasError = result.HasError(new Error(message));

            // Assert
            hasError.Should().BeTrue();
        }

        [Fact]
        public void Contains_error_with_different_casing_message()
        {
            // Arrange
            var result = Result.Fail(new Error("error.message"));

            // Act
            var hasError = result.HasError(new Error("eRroR.mEsSagE"));

            // Assert
            hasError.Should().BeTrue();
        }

        [Fact]
        public void Doesnt_contain_error_with_different_messages()
        {
            // Arrange
            var result = Result.Fail(new Error("error.message"));

            // Act
            var hasError = result.HasError(new Error("different.error.message"));

            // Assert
            hasError.Should().BeFalse();
        }
    }
}

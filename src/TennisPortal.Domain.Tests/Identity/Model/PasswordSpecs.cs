﻿using FluentAssertions;
using System;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Shared.Errors;
using Xunit;

namespace TennisPortal.Domain.Tests.Identity.Model
{
    public class PasswordSpecs
    {
        [Theory]
        [InlineData("Iampassword4")]
        [InlineData("1iAmp455w0rdwitHnumb3rs")]
        [InlineData("p!4@s#S$w%o^r&d*w(i)t.h-+{};\":;',./<>?")]
        [InlineData("-Ten-ch4rs-ten-chars-ten-chars-ten-chars-ten-chars")]
        public void Accepts_properly_formatted_password(string password)
        {
            // Arrange

            // Act
            var passwordOrError = Password.Create(password, password);

            // Assert
            passwordOrError.IsSuccess.Should().BeTrue();
            passwordOrError.Value.Value.Should().Be(password);
        }

        [Fact]
        public void Throws_on_null_password()
        {
            // Arrange

            // Act
            Action creation = () => Password.Create(null!, "Password123");

            // Assert
            creation.Should().Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("password");
        }

        [Fact]
        public void Throws_on_null_password_confirmation()
        {
            // Arrange

            // Act
            Action creation = () => Password.Create("Password123", null!);

            // Assert
            creation.Should().Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("passwordConfirmation");
        }

        [Theory]
        [InlineData("")]
        [InlineData("1")]
        [InlineData("12")]
        [InlineData("123")]
        [InlineData("1234")]
        [InlineData("12345")]
        [InlineData("123456")]
        [InlineData("1234567")]
        public void Rejects_too_short_passwords(string password)
        {
            // Arrange

            // Act
            var passwordOrError = Password.Create(password, password);

            // Assert
            passwordOrError.IsFailed.Should().BeTrue();
            passwordOrError.HasError(FormatErrors.ValueTooShort())
                .Should().BeTrue();
        }

        [Theory]
        [InlineData("-Ten-chars-ten-chars-ten-chars-ten-chars-ten-chars1")]
        [InlineData("-Ten-chars-ten-chars-ten-chars-ten-chars-ten-chars-ten-chars-ten-chars-ten-chars1")]
        public void Rejects_too_long_passwords(string password)
        {// Arrange

            // Act
            var passwordOrError = Password.Create(password, password);

            // Assert
            passwordOrError.IsFailed.Should().BeTrue();
            passwordOrError.HasError(FormatErrors.ValueTooLong())
                .Should().BeTrue();
        }

        [Theory]
        [InlineData("password1")]
        [InlineData("PASSWORD1")]
        [InlineData("PassworD")]
        public void Rejects_passwords_without_upper_case_letter_lower_case_letter_or_digit(string password)
        {
            // Arrange

            // Act
            var passwordOrError = Password.Create(password, password);

            // Assert
            passwordOrError.IsFailed.Should().BeTrue();
            passwordOrError.HasError(FormatErrors.InvalidFormat())
                .Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_the_same_password_are_equal()
        {
            // Arrange
            const string password = "I4mp455w0rd";
            var password1 = Password.Create(password, password).Value;
            var password2 = Password.Create(password, password).Value;

            // Act
            var equals = password1 == password2;

            // Assert
            equals.Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_different_passwords_are_not_equal()
        {
            // Arrange
            var password1 = Password.Create("Password1", "Password1").Value;
            var password2 = Password.Create("Password2", "Password2").Value;

            // Act
            var equals = password1 == password2;

            // Assert
            equals.Should().BeFalse();
        }
    }
}

﻿using FluentAssertions;
using System;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Shared.Errors;
using Xunit;

namespace TennisPortal.Domain.Tests.Identity.Model
{
    public class UsernameSpecs
    {
        [Theory]
        [InlineData("usr")]
        [InlineData("username")]
        [InlineData("uSeRnAmE")]
        [InlineData("1user2name3")]
        [InlineData("XjustbelowcharlimitX")]
        public void Accepts_properly_formatted_username(string username)
        {
            // Arrange

            // Act
            var usernameOrError = Username.Create(username);

            // Assert
            usernameOrError.IsSuccess.Should().BeTrue();
            usernameOrError.Value.Value.Should().Be(username);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("      ")]
        public void Throws_on_null_or_whitespace_username(string username)
        {
            // Arrange

            // Act
            Action creation = () => Username.Create(username);

            // Assert
            creation.Should().Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("username");
        }

        [Theory]
        [InlineData("a")]
        [InlineData("ab")]
        public void Rejects_too_short_usernames(string username)
        {
            // Arrange

            // Act
            var usernameOrError = Username.Create(username);

            // Assert
            usernameOrError.IsFailed.Should().BeTrue();
            usernameOrError.HasError(FormatErrors.ValueTooShort())
                .Should().BeTrue();
        }

        [Theory]
        [InlineData("slightlytoolonguserna")]
        [InlineData("thatisoneextremlytoolongusernameifyouaskmeforopiniononthematter")]
        public void Rejects_too_long_usernames(string username)
        {
            // Arrange

            // Act
            var usernameOrError = Username.Create(username);

            // Assert
            usernameOrError.IsFailed.Should().BeTrue();
            usernameOrError.HasError(FormatErrors.ValueTooLong())
                .Should().BeTrue();
        }

        [Theory]
        [InlineData(" username")]
        [InlineData("username ")]
        [InlineData("user name")]
        [InlineData("use\trname")]
        [InlineData("use\nrname")]
        [InlineData("username.")]
        [InlineData("username/")]
        [InlineData("username'")]
        [InlineData("username;")]
        [InlineData("username[")]
        [InlineData("username-")]
        [InlineData("username=")]
        [InlineData("username}")]
        [InlineData("username?")]
        [InlineData("username<")]
        [InlineData("username!")]
        [InlineData("username@")]
        [InlineData("username$")]
        [InlineData("username%")]
        [InlineData("username(")]
        [InlineData("username~")]
        public void Rejects_usernames_containing_symbol_or_whitespace(string username)
        {
            // Arrange

            // Act
            var usernameOrError = Username.Create(username);

            // Assert
            usernameOrError.IsFailed.Should().BeTrue();
            usernameOrError.HasError(FormatErrors.InvalidFormat())
                .Should().BeTrue();
        }

        [Theory]
        [InlineData("username", "username")]
        [InlineData("uSeRnAmE", "UsErNaMe")]
        public void Two_instances_with_the_same_username_are_equal_regardless_of_casing(
            string username1,
            string username2)
        {
            // Arrange
            var usernameObject1 = Username.Create(username1).Value;
            var usernameObject2 = Username.Create(username2).Value;

            // Act
            var equals = usernameObject1 == usernameObject2;

            // Assert
            equals.Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_different_values_are_not_equal()
        {
            // Arrange
            var username1 = Username.Create("username1");
            var username2 = Username.Create("username2");

            // Act
            var equals = username1 == username2;

            // Assert
            equals.Should().BeFalse();
        }
    }
}

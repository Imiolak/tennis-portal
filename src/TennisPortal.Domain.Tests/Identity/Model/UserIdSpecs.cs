﻿using FluentAssertions;
using System;
using TennisPortal.Domain.Identity.Membership;
using Xunit;

namespace TennisPortal.Domain.Tests.Identity.Model
{
    public class UserIdSpecs
    {
        [Fact]
        public void Accepts_valid_guid()
        {
            // Arrange
            var expectedValue = Guid.NewGuid();

            // Act
            var id = new UserId(expectedValue);

            // Assert
            id.Value.Should().Be(expectedValue);
        }

        [Fact]
        public void Two_instances_with_the_same_value_are_equal()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var id1 = new UserId(guid);
            var id2 = new UserId(guid);

            // Act
            var equals = id1 == id2;

            // Assert
            equals.Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_different_value_are_not_equal()
        {
            // Arrange
            var id1 = new UserId(Guid.NewGuid());
            var id2 = new UserId(Guid.NewGuid());

            // Act
            var equals = id1 == id2;

            // Assert
            equals.Should().BeFalse();
        }
    }
}

﻿using FluentAssertions;
using System;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Shared.Errors;
using Xunit;

namespace TennisPortal.Domain.Tests.Identity.Model
{
    public class EmailAddressSpecs
    {
        [Theory]
        [InlineData("email@domain.com")]
        [InlineData("email+tag@domain.pl")]
        [InlineData("eMaIl@dOmaIn.Co.Uk")]
        [InlineData(" email@domain.com")]
        [InlineData("email@domain.com ")]
        [InlineData(" email@domain.com ")]
        public void Accepts_properly_formatted_email(string expectedEmail)
        {
            // Arrange

            // Act
            var emailOrError = EmailAddress.Create(expectedEmail);

            // Assert
            emailOrError.IsSuccess.Should().BeTrue();
            emailOrError.Value.Value
                .Should().Be(expectedEmail.Trim());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("            ")]
        public void Throws_on_null_or_whitespace_email(string email)
        {
            // Arrange

            // Act
            Action creation = () => EmailAddress.Create(email);

            // Assert
            creation.Should().Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("email");
        }

        [Theory]
        [InlineData("email")]
        [InlineData("email@")]
        [InlineData("email@domain")]
        [InlineData("email@domain.c")]
        [InlineData(".email@domain.com")]
        [InlineData("email.@domain.com")]
        public void Rejects_incorrectly_formatted_email(string email)
        {
            // Arrange

            // Act
            var emailOrError = EmailAddress.Create(email);

            // Assert
            emailOrError.IsFailed.Should().BeTrue();
            emailOrError.HasError(FormatErrors.InvalidFormat())
                .Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_the_same_email_are_equal_regardless_of_casing()
        {
            // Arrange
            var email1 = EmailAddress.Create("eMaIl@domain.com").Value;
            var email2 = EmailAddress.Create("EmAiL@domain.com").Value;

            // Act
            var equals = email1 == email2;

            // Assert
            equals.Should().BeTrue();
        }

        [Fact]
        public void Two_instances_with_different_email_are_not_equal()
        {
            // Arrange
            var email1 = EmailAddress.Create("email1@domain.com").Value;
            var email2 = EmailAddress.Create("email2@domain.com").Value;

            // Act
            var equals = email1 == email2;

            // Assert
            equals.Should().BeFalse();
        }
    }
}

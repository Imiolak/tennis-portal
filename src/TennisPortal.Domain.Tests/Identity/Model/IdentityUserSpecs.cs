﻿using FluentAssertions;
using System;
using TennisPortal.Domain.Identity.Membership;
using Xunit;

namespace TennisPortal.Domain.Tests.Identity.Model
{
    public class IdentityUserSpecs
    {
        private readonly Username _validUsername;
        private readonly EmailAddress _validEmail;
        private readonly string _validHashedPassword;

        public IdentityUserSpecs()
        {
            _validUsername = Username.Create("username").Value;
            _validEmail = EmailAddress.Create("email@domain.com").Value;
            _validHashedPassword = "#";
        }

        [Fact]
        public void New_user_raises_created_event()
        {
            // Arrange

            // Act
            var user = new IdentityUser(
                _validUsername,
                _validEmail,
                _validHashedPassword);

            // Arrange
            user.RaisedEvents.Count.Should().Be(1);
            user.RaisedEvents[0].Should().BeOfType<UserCreatedEvent>();

            var @event = (UserCreatedEvent)user.RaisedEvents[0];
            @event.UserId.Value.Should().NotBe(Guid.Empty);
            @event.Username.Should().Be(_validUsername);
            @event.Email.Should().Be(_validEmail);
        }

        [Fact]
        public void Generates_id()
        {
            // Arrange

            // Act
            var user = new IdentityUser(
                _validUsername,
                _validEmail,
                _validHashedPassword);

            // Arrange
            user.Id.Should().NotBeNull();
            user.Id.Value.Should().NotBe(Guid.Empty);
        }

        [Fact]
        public void Assigns_username()
        {
            // Arrange

            // Act
            var user = new IdentityUser(
                _validUsername,
                _validEmail,
                _validHashedPassword);

            // Arrange
            user.Username.Should().Be(_validUsername);
        }

        [Fact]
        public void Throws_on_null_username()
        {
            // Arrange

            // Act
            Action constructor = () => new IdentityUser(
                null!,
                _validEmail,
                _validHashedPassword);

            // Assert
            constructor.Should()
                .Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("username");
        }

        [Fact]
        public void Assigns_email()
        {
            // Arrange

            // Act
            var user = new IdentityUser(
                _validUsername,
                _validEmail,
                _validHashedPassword);

            // Arrange
            user.Email.Should().Be(_validEmail);
        }

        [Fact]
        public void Throws_on_null_email()
        {
            // Arrange

            // Act
            Action constructor = () => new IdentityUser(
                _validUsername,
                null!,
                _validHashedPassword);

            // Assert
            constructor.Should()
                .Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("email");
        }

        [Fact]
        public void Assigns_hashed_password()
        {
            // Arrange

            // Act
            var user = new IdentityUser(
                _validUsername,
                _validEmail,
                _validHashedPassword);

            // Arrange
            user.HashedPassword.Should().Be(_validHashedPassword);
        }

        [Fact]
        public void Throws_on_null_hashed_password()
        {
            // Arrange

            // Act
            Action constructor = () => new IdentityUser(
                _validUsername,
                null!,
                _validHashedPassword);

            // Assert
            constructor.Should()
                .Throw<ArgumentNullException>()
                .And
                .ParamName.Should().Be("email");
        }
    }
}

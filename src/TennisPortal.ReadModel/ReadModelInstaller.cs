﻿using Microsoft.Extensions.DependencyInjection;
using TennisPortal.ReadModel.Identity.Membership;

namespace TennisPortal.ReadModel
{
    public static class ReadModelInstaller
    {
        public static void AddReadModel(
            this IServiceCollection services,
            string dbConnectionString)
        {
            services.AddSingleton(_ => new ReadConnectionStrings(dbConnectionString));
            services.AddScoped<UserReadRepository>();
        }
    }
}

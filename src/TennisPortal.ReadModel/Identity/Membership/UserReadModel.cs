﻿namespace TennisPortal.ReadModel.Identity.Membership
{
    public class UserReadModel
    {
        public UserReadModel(
            string username,
            string email)
        {
            Username = username;
            Email = email;
        }

        public string Username { get; }
        public string Email { get; }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using TennisPortal.Cqrs;

namespace TennisPortal.ReadModel.Identity.Membership
{
    public class GetUserByIdQueryHandler : IQueryHandler<GetUserByIdQuery, UserReadModel?>
    {
        private readonly UserReadRepository _users;

        public GetUserByIdQueryHandler(UserReadRepository users)
        {
            _users = users;
        }

        public async Task<UserReadModel?> Handle(
            GetUserByIdQuery request,
            CancellationToken cancellationToken)
        {
            return await _users.ById(request.UserId);
        }
    }
}

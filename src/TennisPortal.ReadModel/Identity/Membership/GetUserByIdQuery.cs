﻿using System;
using TennisPortal.Cqrs;

namespace TennisPortal.ReadModel.Identity.Membership
{
    public class GetUserByIdQuery : IQuery<UserReadModel?>
    {
        public GetUserByIdQuery(Guid userId)
        {
            UserId = userId;
        }

        public Guid UserId { get; }
    }
}

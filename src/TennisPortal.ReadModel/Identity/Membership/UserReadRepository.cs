﻿using Dapper;
using System;
using System.Threading.Tasks;

namespace TennisPortal.ReadModel.Identity.Membership
{
    public class UserReadRepository : ReadRepositoryBase
    {
        public UserReadRepository(ReadConnectionStrings connectionStrings)
            : base(connectionStrings)
        {
        }

        public async Task<UserReadModel?> ById(Guid userId)
        {
            using var connection = OpenDbConnection();

            return await connection.QuerySingleOrDefaultAsync<UserReadModel>(
                @"SELECT Username, Email
                    FROM Users
                    WHERE Id = @Id;",
                new
                {
                    Id = userId.ToString()
                }
            );
        }
    }
}

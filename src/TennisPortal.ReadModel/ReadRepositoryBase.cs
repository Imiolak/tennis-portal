﻿using System.Data.Common;
using System.Data.SqlClient;

namespace TennisPortal.ReadModel
{
    public class ReadRepositoryBase
    {
        private string _connectionString;

        public ReadRepositoryBase(ReadConnectionStrings connectionStrings)
        {
            _connectionString = connectionStrings.TennisPortalDb;
        }

        protected DbConnection OpenDbConnection() => new SqlConnection(_connectionString);
    }
}

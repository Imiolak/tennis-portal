﻿namespace TennisPortal.ReadModel
{
    public class ReadConnectionStrings
    {
        public ReadConnectionStrings(string tennisPortalDb)
        {
            TennisPortalDb = tennisPortalDb;
        }

        public string TennisPortalDb { get; }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using TennisPortal.Domain.Identity.Membership.Repositories;
using TennisPortal.Domain.Identity.Membership.Services.Repositories;
using TennisPortal.Infrastructure.Identity.Membership;
using TennisPortal.Infrastructure.Identity.Repositories;

namespace TennisPortal.Infrastructure.Identity
{
    public static class IdentityModuleInstaller
    {
        public static void InstallIdentityModule(this IServiceCollection services)
        {
            services.AddSingleton<IPasswordHashService, BCryptPasswordHashService>();
            services.AddScoped<IUserRepository, EfUserRepository>();
        }
    }
}

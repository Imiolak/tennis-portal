﻿using System.Linq;
using System.Threading.Tasks;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Identity.Membership.Services.Repositories;
using TennisPortal.Infrastructure.DataAccess;

namespace TennisPortal.Infrastructure.Identity.Repositories
{
    public class EfUserRepository : IUserRepository
    {
        private readonly TennisPortalDbContext _context;

        public EfUserRepository(TennisPortalDbContext context)
        {
            _context = context;
        }

        public async Task Add(IdentityUser newUser)
        {
            await _context.Users.AddAsync(newUser);
        }

        public Task<bool> ContainsEmail(EmailAddress email)
        {
            return Task.FromResult(
                _context.Users.Any(user => user.Email == email));
        }

        public Task<bool> ContainsUsername(Username username)
        {
            return Task.FromResult(
                _context.Users.Any(user => user.Username == username));
        }
    }
}

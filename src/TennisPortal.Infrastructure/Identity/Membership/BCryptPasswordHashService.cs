﻿using System.Threading.Tasks;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Identity.Membership.Repositories;

namespace TennisPortal.Infrastructure.Identity.Membership
{
    public class BCryptPasswordHashService : IPasswordHashService
    {
        public Task<string> HashPassword(Password password)
        {
            return Task.FromResult(
                BCrypt.Net.BCrypt.HashPassword(
                    password.Value));
        }

        public Task<bool> VerifyPassword(string hashedPassword, string password)
        {
            return Task.FromResult(
                BCrypt.Net.BCrypt.Verify(
                    password,
                    hashedPassword));
        }
    }
}

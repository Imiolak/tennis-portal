﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Reflection;
using TennisPortal.Cqrs;
using TennisPortal.Domain.DDD;
using TennisPortal.Infrastructure.DataAccess;
using TennisPortal.ReadModel;

namespace TennisPortal.Infrastructure
{
    public static class InfractructureInstaller
    {
        public static void InstallInfrastructure(
            this IServiceCollection services,
            IOptions<ConnectionStringSettings> connectionStrings)
        {
            InstallDb(services, connectionStrings);
            InstallCqrs(services);
        }

        private static void InstallCqrs(IServiceCollection services)
        {
            services.AddCqrs(
                Assembly.Load(
                    new AssemblyName("TennisPortal.Application")),
                Assembly.Load(
                    new AssemblyName("TennisPortal.ReadModel")));
        }

        private static void InstallDb(
            IServiceCollection services,
            IOptions<ConnectionStringSettings> connectionStrings)
        {
            services.AddDbContext<TennisPortalDbContext>(options =>
            {
                options.UseSqlServer(connectionStrings.Value.TennisPortalDb);
            });

            services.AddScoped<IUnitOfWork, EfUnitOfWork>();
            services.AddReadModel(connectionStrings.Value.TennisPortalReadDb);
        }
    }
}

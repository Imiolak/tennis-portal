﻿#nullable disable
namespace TennisPortal.Infrastructure.DataAccess
{
    public class ConnectionStringSettings
    {
        public string TennisPortalDb { get; set; }
        public string TennisPortalReadDb { get; set; }
    }
}
#nullable restore

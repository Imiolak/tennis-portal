﻿using System.Linq;
using System.Threading.Tasks;
using TennisPortal.Cqrs;
using TennisPortal.Domain.DDD;

namespace TennisPortal.Infrastructure.DataAccess
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly TennisPortalDbContext _context;
        private readonly IEventDispatcher _eventPublisher;

        public EfUnitOfWork(
            TennisPortalDbContext context,
            IEventDispatcher eventPublisher)
        {
            _context = context;
            _eventPublisher = eventPublisher;
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();

            var aggregates = _context.ChangeTracker
                .Entries()
                .Where(entry => entry.Entity is IAggregateRoot)
                .Select(entry => (IAggregateRoot)entry.Entity)
                .Where(aggregate => aggregate.RaisedEvents.Any());

            foreach (var aggregate in aggregates)
            {
                await _eventPublisher.Dispatch(aggregate.RaisedEvents);
                aggregate.ClearEvents();
            }
        }
    }
}

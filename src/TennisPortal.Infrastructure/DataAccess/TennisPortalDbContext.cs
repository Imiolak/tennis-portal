﻿#nullable disable
using Microsoft.EntityFrameworkCore;
using TennisPortal.Domain.Identity.Membership;

namespace TennisPortal.Infrastructure.DataAccess
{
    public class TennisPortalDbContext : DbContext
    {
        public TennisPortalDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<IdentityUser> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CreateUsersTable(modelBuilder);
        }

        private void CreateUsersTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUser>(user =>
            {
                user.HasKey(user => user.Id);

                user.Property(user => user.Id)
                    .HasConversion(
                        id => id.Value,
                        id => new UserId(id));

                user.Property(user => user.Username)
                    .HasMaxLength(20)
                    .HasConversion(
                        username => username.Value,
                        username => Username.Create(username).Value);

                user.HasIndex(user => user.Username)
                    .IsUnique(true);

                user.Property(user => user.Email)
                    .HasMaxLength(50)
                    .HasConversion(
                        email => email.Value,
                        email => EmailAddress.Create(email).Value);

                user.HasIndex(user => user.Email)
                    .IsUnique(true);

                user.Property(user => user.HashedPassword);
            });
        }
    }
}
#nullable restore

import { createAction } from "@reduxjs/toolkit";

export enum ApiActionTypes {
    GET = "API Get",
    POST = "API Post",
    SUCCESS = "API Success",
    FAILURE = "API Failure"
}

export const apiActions = {
    get: (callingActionType: string) =>
        createAction(
            `${callingActionType} | ${ApiActionTypes.GET}`,
            (url: string) => ({
                payload: {
                    url
                },
                meta: {
                    context: callingActionType
                }
            })
        ),
    post: (callingActionType: string) =>
        createAction(
            `${callingActionType} | ${ApiActionTypes.POST}`,
            (url: string, body?: any) => ({
                payload: {
                    url,
                    body
                },
                meta: {
                    context: callingActionType
                }
            })
        ),
    success: (callingActionType: string) =>
        createAction<any>(`${callingActionType} | ${ApiActionTypes.SUCCESS}`),
    failure: (callingActionType: string) =>
        createAction<any>(`${callingActionType} | ${ApiActionTypes.FAILURE}`)
};

import { Middleware } from "redux";
import axios from "axios";
import { apiActions, ApiActionTypes } from "./api.actions";

const get: Middleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (!action.type.includes(ApiActionTypes.GET)) {
        return;
    }

    const { url } = action.payload;
    const { context } = action.meta;

    axios
        .get(url)
        .then((response) => {
            dispatch(apiActions.success(context)(response.data));
        })
        .catch((error) => dispatch(apiActions.failure(context)(error)));
};

const post: Middleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (!action.type.includes(ApiActionTypes.POST)) {
        return;
    }

    const { url, body } = action.payload;
    const { context } = action.meta;

    axios
        .post(url, body)
        .then((response) => {
            dispatch(apiActions.success(context)(response.data));
        })
        .catch((error) => dispatch(apiActions.failure(context)(error)));
};

export const apiMiddleware = [get, post];

import React from "react";
import { useForm } from "react-hook-form";
import { Input, Form, FormGroup } from "components";
import { CreateAccountRequest } from "services/membership/models/CreateAccountRequest";
import SubmitButton from "./SubmitButton";

export interface RegistrationFormProps {
    registrationInProgress: boolean;
    registrationSucceeded: boolean;
    onSubmit: (registrationForm: CreateAccountRequest) => void;
}

const RegistrationForm = ({
    registrationInProgress,
    registrationSucceeded,
    onSubmit
}: RegistrationFormProps) => {
    const { register, handleSubmit, errors, getValues } = useForm<
        CreateAccountRequest
    >();

    const validatePasswordConfirmation = (passwordConfirmation: string) =>
        passwordConfirmation === getValues().password
            ? undefined
            : "Must match password";

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormGroup>
                <Input
                    invalid={!!errors.username}
                    id="username"
                    name="username"
                    type="text"
                    label="Username"
                    hint="You'll use this to log in"
                    error={errors.username?.message?.toString()}
                    innerRef={register({
                        required:
                            "Must be at least 3 and at most 20 characters long",
                        minLength: {
                            value: 3,
                            message:
                                "Must be at least 3 and at most 20 characters long"
                        },
                        maxLength: {
                            value: 20,
                            message:
                                "Must be at least 3 and at most 20 characters long"
                        }
                    })}
                />
            </FormGroup>
            <FormGroup>
                <Input
                    invalid={!!errors.email}
                    id="email"
                    name="email"
                    type="email"
                    label="Email"
                    hint="Used only for password reset"
                    error={errors.email?.message?.toString()}
                    innerRef={register({ required: "Provide an email" })}
                />
            </FormGroup>
            <FormGroup>
                <Input
                    invalid={!!errors.password}
                    id="password"
                    name="password"
                    type="password"
                    label="Password"
                    error={errors.password?.message?.toString()}
                    innerRef={register({
                        required: "Must be at least 8 characters long",
                        minLength: {
                            value: 8,
                            message: "Must be at least 8 characters long"
                        }
                    })}
                />
            </FormGroup>
            <FormGroup>
                <Input
                    invalid={!!errors.passwordConfirmation}
                    id="passwordConfirmation"
                    name="passwordConfirmation"
                    type="password"
                    label="Password confirmation"
                    error={errors.passwordConfirmation?.message?.toString()}
                    innerRef={register({
                        validate: validatePasswordConfirmation
                    })}
                />
            </FormGroup>
            <SubmitButton submissionInProgress={registrationInProgress}>
                {registrationSucceeded ? "Created!" : "Create an account"}
            </SubmitButton>
        </Form>
    );
};

export default RegistrationForm;

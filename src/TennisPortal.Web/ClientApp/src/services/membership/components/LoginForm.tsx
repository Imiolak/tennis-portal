import { Form, FormGroup, Input } from "components";
import React from "react";
import { useForm } from "react-hook-form";
import { UserLoginRequest } from "services/membership/models/UserLoginRequest";
import SubmitButton from "./SubmitButton";

const LoginForm = () => {
    const { handleSubmit, register, errors } = useForm<UserLoginRequest>();

    const onSubmit = (loginForm: UserLoginRequest) => {};

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormGroup>
                <Input
                    invalid={!!errors.username}
                    id="username"
                    name="username"
                    type="text"
                    label="Username"
                    error={errors.username?.message?.toString()}
                    innerRef={register({
                        required: "Please provide password"
                    })}
                />
            </FormGroup>
            <FormGroup>
                <Input
                    invalid={!!errors.password}
                    id="password"
                    name="password"
                    type="password"
                    label="Password"
                    error={errors.password?.message?.toString()}
                    innerRef={register({
                        required: "Please provide password"
                    })}
                />
            </FormGroup>
            <SubmitButton submissionInProgress={false}>Sign in</SubmitButton>
        </Form>
    );
};

export default LoginForm;

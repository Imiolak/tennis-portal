import React, { PropsWithChildren } from "react";
import { Button } from "components";
import { Spinner } from "reactstrap";

interface SubmitButtonProps {
    submissionInProgress: boolean;
}

const SubmitButton = ({
    submissionInProgress,
    children
}: PropsWithChildren<SubmitButtonProps>) => {
    return (
        <Button
            disabled={submissionInProgress}
            color="primary"
            className="w-100 mt-2"
        >
            {submissionInProgress ? <Spinner size="sm" /> : children}
        </Button>
    );
};

export default SubmitButton;

import "./MembershipForm.css";
import React, { useState, PropsWithChildren } from "react";
import {
    Container,
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane
} from "reactstrap";
import LoginForm from "./LoginForm";
import RegistrationForm from "./RegistrationForm";
import { useSelector, useDispatch } from "react-redux";
import * as MembershipStore from "services/membership/store";
import { CreateAccountRequest } from "services/membership/models/CreateAccountRequest";

const TabIds = {
    login: "login",
    register: "register"
};

const MembershipForm = () => {
    const [activeTab, setActiveTab] = useState(TabIds.login);
    const dispatch = useDispatch();

    const registrationInProgress = useSelector(
        MembershipStore.selectors.registrationInProgress
    );

    const registrationSucceeded = useSelector(
        MembershipStore.selectors.registrationSucceeded
    );

    const sendRegistrationForm = (registrationForm: CreateAccountRequest) => {
        dispatch(
            MembershipStore.actions.sendRegistrationForm(registrationForm)
        );
    };

    const TabLink = ({
        tabId,
        children
    }: PropsWithChildren<{ tabId: string }>) => (
        <NavLink
            active={activeTab === tabId}
            onClick={() => setActiveTab(tabId)}
        >
            {children}
        </NavLink>
    );

    return (
        <Container className="membership-form">
            <Nav tabs>
                <NavItem>
                    <TabLink tabId={TabIds.login}>Sign in</TabLink>
                </NavItem>
                <NavItem>
                    <TabLink tabId={TabIds.register}>Register</TabLink>
                </NavItem>
            </Nav>
            <TabContent
                className="border border-top-0 rounded-bottom p-4"
                activeTab={activeTab}
            >
                <TabPane tabId={TabIds.login}>
                    <LoginForm></LoginForm>
                </TabPane>
                <TabPane tabId={TabIds.register}>
                    <RegistrationForm
                        registrationInProgress={registrationInProgress}
                        registrationSucceeded={registrationSucceeded}
                        onSubmit={sendRegistrationForm}
                    ></RegistrationForm>
                </TabPane>
            </TabContent>
        </Container>
    );
};

export default MembershipForm;

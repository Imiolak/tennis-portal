export enum RegistrationStatus {
    Idle = "Idle",
    Active = "Active",
    Success = "Success",
    Failure = "Failure"
}

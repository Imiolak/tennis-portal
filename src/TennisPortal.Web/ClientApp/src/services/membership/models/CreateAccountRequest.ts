export interface CreateAccountRequest {
    username: string;
    email: string;
    password: string;
    passwordConfirmation: string;
}

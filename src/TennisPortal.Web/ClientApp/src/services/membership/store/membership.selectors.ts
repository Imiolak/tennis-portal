import { createSelector } from "@reduxjs/toolkit";
import { RegistrationStatus } from "../models/RegistrationStatus";
import { ApplicationState } from "ApplicationStore";

const stateSelector = (applicationState: ApplicationState) =>
    applicationState.membership;

export const membersipSelectors = {
    registrationInProgress: createSelector(
        stateSelector,
        (membershipState) =>
            membershipState.registrationStatus === RegistrationStatus.Active
    ),
    registrationSucceeded: createSelector(
        stateSelector,
        (membershipState) =>
            membershipState.registrationStatus === RegistrationStatus.Success
    ),
};

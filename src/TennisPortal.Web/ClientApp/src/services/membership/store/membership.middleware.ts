import { Middleware } from "redux";
import * as Api from "services/api/store";
import { membershipActions } from "./membership.actions";

const createAccount: Middleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (!membershipActions.sendRegistrationForm.match(action)) {
        return;
    }

    dispatch(Api.actions.post(action.type)("/api/users", action.payload));
};

const accountCreated: Middleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (
        !Api.actions
            .success(membershipActions.sendRegistrationForm.type)
            .match(action)
    ) {
        return;
    }

    dispatch(membershipActions.accountCreated());
};

const accountCreationFailed: Middleware = ({ dispatch }) => (next) => (
    action
) => {
    next(action);

    if (
        !Api.actions
            .failure(membershipActions.sendRegistrationForm.type)
            .match(action)
    ) {
        return;
    }

    dispatch(membershipActions.accountCreationFailed());
};

export const membershipMiddleware = [
    createAccount,
    accountCreated,
    accountCreationFailed
];

import { RegistrationStatus } from "services/membership/models/RegistrationStatus";

export interface MembershipState {
    registrationStatus: RegistrationStatus;
}

export const defaultState: MembershipState = {
    registrationStatus: RegistrationStatus.Idle
};

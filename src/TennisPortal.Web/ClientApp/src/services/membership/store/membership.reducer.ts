import { createReducer } from "@reduxjs/toolkit";
import { MembershipState, defaultState } from "./membership.state";
import { membershipActions } from "./membership.actions";
import { RegistrationStatus } from "../models/RegistrationStatus";

export const membershipReducer = createReducer<MembershipState>(defaultState, {
    [membershipActions.sendRegistrationForm.type]: (state, action) => {
        return {
            ...state,
            registrationStatus: RegistrationStatus.Active
        };
    },
    [membershipActions.accountCreated.type]: (state, action) => ({
        ...state,
        registrationStatus: RegistrationStatus.Success
    }),
    [membershipActions.accountCreationFailed.type]: (state, action) => ({
        ...state,
        registrationStatus: RegistrationStatus.Failure
    })
});

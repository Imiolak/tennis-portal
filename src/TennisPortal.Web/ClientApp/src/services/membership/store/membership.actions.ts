import { createAction } from "@reduxjs/toolkit";
import { CreateAccountRequest } from "services/membership/models/CreateAccountRequest";

export const MEMBERSHIP_CONTEXT = "[Membership]";
export const ActionTypes = {
    SEND_REGISTRATION_FORM: `${MEMBERSHIP_CONTEXT} Send Registration Form`,
    ACCOUNT_CREATED: `${MEMBERSHIP_CONTEXT} Account Created`,
    ACCOUNT_CREATION_FAILED: `${MEMBERSHIP_CONTEXT} Account Creation Failed`
};

export const membershipActions = {
    sendRegistrationForm: createAction<CreateAccountRequest>(
        ActionTypes.SEND_REGISTRATION_FORM
    ),
    accountCreated: createAction(ActionTypes.ACCOUNT_CREATED),
    accountCreationFailed: createAction(ActionTypes.ACCOUNT_CREATION_FAILED)
};

import React, { HTMLAttributes } from "react";
import { Form as ReactstrapForm } from "reactstrap";

export interface FormProps extends HTMLAttributes<HTMLFormElement> {}

const Form = (props: FormProps) => {
    return <ReactstrapForm {...props}></ReactstrapForm>;
};

export default Form;

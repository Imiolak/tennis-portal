import React, { ButtonHTMLAttributes } from "react";
import { Button as ReactstrapButton } from "reactstrap";

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

const Button = (props: ButtonProps) => {
    return <ReactstrapButton {...props} />;
};

export default Button;

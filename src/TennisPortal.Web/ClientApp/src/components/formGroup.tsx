import React, { HTMLAttributes } from "react";
import { FormGroup as ReactstrapFormGroup } from "reactstrap";

export interface FormGroupProps extends HTMLAttributes<HTMLDivElement> {}

const FormGroup = (props: FormGroupProps) => {
    return <ReactstrapFormGroup {...props} />;
};

export default FormGroup;

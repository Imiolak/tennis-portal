import React, { InputHTMLAttributes } from "react";
import {
    Input as ReactstrapInput,
    Label as ReactstrapLabel,
    FormFeedback as ReactstrapFormFeedback,
    FormText as ReactstrapFormText
} from "reactstrap";

type InputType =
    | "text"
    | "email"
    | "select"
    | "file"
    | "radio"
    | "checkbox"
    | "textarea"
    | "button"
    | "reset"
    | "submit"
    | "date"
    | "datetime-local"
    | "hidden"
    | "image"
    | "month"
    | "number"
    | "range"
    | "search"
    | "tel"
    | "url"
    | "week"
    | "password"
    | "datetime"
    | "time"
    | "color";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    id: string;
    name: string;
    type: InputType;
    label?: string;
    hint?: string;
    error?: string;
    invalid?: boolean;
    innerRef?: React.Ref<HTMLInputElement>;
}

const Input = ({ label, hint, error, ...props }: InputProps) => {
    return (
        <>
            {!!label && (
                <ReactstrapLabel for={props.id}>{label}</ReactstrapLabel>
            )}
            <ReactstrapInput {...props} />
            <ReactstrapFormFeedback invalid>{error}</ReactstrapFormFeedback>
            {!!hint && (
                <ReactstrapFormText color="muted">{hint}</ReactstrapFormText>
            )}
        </>
    );
};

export default Input;

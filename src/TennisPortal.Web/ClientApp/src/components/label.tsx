import React, { LabelHTMLAttributes } from "react";
import { Label as ReactstrapLabel } from "reactstrap";

export interface LabelProps extends LabelHTMLAttributes<HTMLLabelElement> {
    for: string;
}

const Label = (props: LabelProps) => {
    return <ReactstrapLabel {...props} />;
};

export default Label;

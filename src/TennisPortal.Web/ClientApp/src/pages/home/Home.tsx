import * as React from "react";
import { Container } from "reactstrap";

const Home = () => (
    <Container>
        <h1>Home</h1>
    </Container>
);

export default Home;

import React from "react";
import "./NavBar.css";
import { Navbar, Container, NavbarBrand, NavItem, NavLink } from "reactstrap";
import { Link } from "react-router-dom";

const NavBar = () => {
    return (
        <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light>
                <Container>
                    <ul className="navbar-nav flex-grow">
                        <NavbarBrand tag={Link} to="/">TennisPortal</NavbarBrand>
                        {/* <NavItem>
                            <NavLink tag={Link} className="text-dark" to="/">
                                Home
                            </NavLink>
                        </NavItem> */}
                    </ul>
                    <ul className="navbar-nav flex-grow">
                        <NavItem>
                            <NavLink tag={Link} className="text-dark" to="/login">
                                Log in / Register
                            </NavLink>
                        </NavItem>
                    </ul>
                </Container>
            </Navbar>
        </header>
    );
}

export default NavBar;

import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { History } from "history";
import { connectRouter, routerMiddleware } from "connected-react-router";
import * as ApiStore from "services/api/store";
import * as MembershipStore from "services/membership/store";

export interface ApplicationState {
    membership: MembershipStore.MembershipState;
}

export const initialState: ApplicationState = {
    membership: MembershipStore.defaultState
};

export function createStore(history: History) {
    const reducers = combineReducers({
        membership: MembershipStore.reducer,
        router: connectRouter(history)
    });

    const middleware = [
        ...MembershipStore.middleware,
        ...ApiStore.middleware,
        routerMiddleware(history)
    ];

    return configureStore({
        reducer: reducers,
        middleware: middleware,
        devTools: process.env.NODE_ENV !== "production",
        preloadedState: initialState
    });
}

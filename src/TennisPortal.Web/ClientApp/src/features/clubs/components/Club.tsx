import React from "react";
import { RouteComponentProps } from "react-router";

type ClubProps = RouteComponentProps<{clubShortName: string}>;

export const Club = ({match}: ClubProps) => {
    return <div>{match.params.clubShortName}</div>
}

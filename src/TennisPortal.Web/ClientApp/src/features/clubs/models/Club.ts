export interface Club {
    fullName: string;
    shortName: string;
}

import { HomeState, homeStateDefault } from "./clubs-state";
import { Reducer } from "redux";
import * as HomeActions from "./clubs-actions";

export const clubsReducer: Reducer<HomeState> = (
    state: HomeState | undefined = homeStateDefault,
    action: HomeActions.All
): HomeState => {
    switch (action.type) {
        case HomeActions.GET_CLUBS: {
            return {
                ...state,
                loadingClubs: true
            };
        }

        case HomeActions.SET_CLUBS: {
            const setClubsAction = action as HomeActions.SetClubsAction;
            return {
                ...state,
                loadingClubs: false,
                clubs: setClubsAction.clubs
            };
        }

        default: {
            return state;
        }
    }
};

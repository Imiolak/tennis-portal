import { Club } from "../../../features/clubs/models/Club";

export interface HomeState {
    loadingClubs: boolean;
    clubs: Club[];
}

export const homeStateDefault: HomeState = {
    loadingClubs: false,
    clubs: []
};

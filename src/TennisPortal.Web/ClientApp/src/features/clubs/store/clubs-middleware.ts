import { Middleware } from "redux";

const CLUBS_URL = "/api/clubs";

const getClubsFlow: Middleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    // switch (action.type) {
    //     case GET_CLUBS:
    //         // dispatch(ApiActionCreators.apiRequest(
    //         //     ClubsContext,
    //         //     CLUBS_URL,
    //         //     "GET",
    //         // ));

    //         dispatch(ClubsActionCreators.setClubs([]));
    //         break;

    //     case `${ClubsContext} ${ApiActionTypes.API_SUCCESS}`:
    //         const clubsResponse = action as ApiSuccessAction<Club[]>;
    //         dispatch(ClubsActionCreators.setClubs(clubsResponse.data));
    //         break;
    // }
};

export const homeMiddleware = [getClubsFlow];

import { Club } from "../../../features/clubs/models/Club";
import { Action } from "redux";

export const ClubsContext = "[Clubs]";
export const GET_CLUBS = `${ClubsContext} Get`;
export const SET_CLUBS = `${ClubsContext} Set`;

export type All = GetClubsAction | SetClubsAction;

export interface GetClubsAction extends Action {}

export interface SetClubsAction extends Action {
    clubs: Club[];
}

export const ClubsActionCreators = {
    getClubs: (): GetClubsAction => ({
        type: GET_CLUBS
    }),
    setClubs: (clubs: Club[]): SetClubsAction => ({
        type: SET_CLUBS,
        clubs
    })
};

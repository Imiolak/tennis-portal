import React from "react";
import { Container } from "reactstrap";
import NavBar from "./pages/home/NavBar";

export default (props: { children?: React.ReactNode }) => (
    <>
        <NavBar />
        <Container>{props.children}</Container>
    </>
);

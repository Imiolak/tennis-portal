import React from "react";
import { Route, Switch } from "react-router";
import Layout from "./Layout";
import Home from "pages/home/Home";
import MembershipForm from "services/membership/components/MembershipForm";
import { Club } from "features/clubs/components/Club";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={MembershipForm} />
                <Route path="/:clubShortName" component={Club} />
            </Switch>
        </Layout>
    );
};

export default App;

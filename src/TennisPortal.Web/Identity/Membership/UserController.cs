﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TennisPortal.Application.Identity.Membership;
using TennisPortal.Cqrs;
using TennisPortal.Domain.Identity.Membership;
using TennisPortal.Domain.Shared.Errors;
using TennisPortal.ReadModel.Identity.Membership;
using TennisPortal.Web.Identity.Membership.Dto;

namespace TennisPortal.Web.Identity.Membership
{
    [ApiController]
    [Route("api/users")]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public UserController(
            ICommandBus commandBus,
            IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet("{userId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserById([FromRoute] Guid userId)
        {
            var query = new GetUserByIdQuery(userId);
            var maybeUser = await _queryBus.Execute(query);

            return maybeUser != null
                ? Ok(maybeUser!) as IActionResult
                : NotFound();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserRequest createUserRequest)
        {
            var command = new CreateUserCommand(
                createUserRequest.Username,
                createUserRequest.Email,
                createUserRequest.Password,
                createUserRequest.PasswordConfirmation);

            var result = await _commandBus.Execute(command);

            if (result.IsFailed)
            {
                if (result.HasError(MembershipErrors.UsernameNotUnique())
                    || result.HasError(MembershipErrors.EmailNotUnique()))
                {
                    return Conflict();
                }

                return BadRequest();
            }

            return Created($"/{result.Value}", null);
        }
    }
}

﻿#nullable disable
using System.ComponentModel.DataAnnotations;

namespace TennisPortal.Web.Identity.Membership.Dto
{
    public class CreateUserRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string PasswordConfirmation { get; set; }
    }
}
#nullable restore

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using TennisPortal.Infrastructure;
using TennisPortal.Infrastructure.DataAccess;
using TennisPortal.Infrastructure.Identity;

namespace TennisPortal.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfiureSettings(services);
            ConfigureInfrastructure(services);
            ConfigureModules(services);
            ConfigureSwagger(services);

            services.AddControllers();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        private void ConfiureSettings(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<ConnectionStringSettings>(
                Configuration.GetSection("ConnectionStrings"));
        }

        private void ConfigureInfrastructure(IServiceCollection services)
        {
            var connectionStrings = services.BuildServiceProvider()
                .GetService<IOptions<ConnectionStringSettings>>();

            services.InstallInfrastructure(connectionStrings);
        }

        private void ConfigureModules(IServiceCollection services)
        {
            services.InstallIdentityModule();
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    "v1",
                    new OpenApiInfo
                    {
                        Title = "Tennis Portal API",
                        Version = "v1"
                    });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseRouting();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tennis Portal API v1");
                c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
            });
        }
    }
}

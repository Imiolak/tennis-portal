﻿using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface IQueryBus
    {
        Task<TResult> Execute<TResult>(IQuery<TResult> query);
    }
}

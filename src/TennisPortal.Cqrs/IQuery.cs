﻿using MediatR;

namespace TennisPortal.Cqrs
{
    public interface IQuery<TResult> : IRequest<TResult>
    {
    }
}

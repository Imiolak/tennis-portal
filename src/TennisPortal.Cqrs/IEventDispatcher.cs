﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface IEventDispatcher
    {
        Task Dispatch(IReadOnlyCollection<IDomainEvent> events);
    }
}

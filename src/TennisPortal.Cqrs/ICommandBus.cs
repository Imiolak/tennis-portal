﻿using FluentResults;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface ICommandBus
    {
        Task<Result> Execute(ICommand command);

        Task<Result<TResult>> Execute<TResult>(ICommand<TResult> command);
    }
}

﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface IEventHandler<TEvent> : INotificationHandler<TEvent>
        where TEvent : IDomainEvent
    {
        new Task Handle(TEvent @event, CancellationToken cancellationToken);

        async Task INotificationHandler<TEvent>.Handle(
            TEvent notification,
            CancellationToken cancellationToken)
        {
            await Handle(notification, cancellationToken);
        }
    }
}

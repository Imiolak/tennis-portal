﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace TennisPortal.Cqrs
{
    public static class IServiceCollectionExtensions
    {
        public static void AddCqrs(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.AddMediatR(assemblies);
            services.AddScoped<ICommandBus, MediatorCommandBus>();
            services.AddScoped<IQueryBus, MediatorQueryBus>();
            services.AddScoped<IEventDispatcher, MediatorEventDispatcher>();
        }
    }
}

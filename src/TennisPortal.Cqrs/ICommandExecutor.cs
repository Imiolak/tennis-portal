﻿using FluentResults;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface ICommandExecutor<TCommand> : IRequestHandler<TCommand, Result>
        where TCommand : ICommand
    {
        Task<Result> Execute(TCommand command, CancellationToken cancellationToken);

        async Task<Result> IRequestHandler<TCommand, Result>.Handle(
            TCommand request,
            CancellationToken cancellationToken)
        {
            return await Execute(request, cancellationToken);
        }
    }

    public interface ICommandExecutor<TCommand, TResult> : IRequestHandler<TCommand, Result<TResult>>
        where TCommand : ICommand<TResult>
    {
        Task<Result<TResult>> Execute(TCommand command, CancellationToken cancellationToken);

        async Task<Result<TResult>> IRequestHandler<TCommand, Result<TResult>>.Handle(
            TCommand request,
            CancellationToken cancellationToken)
        {
            return await Execute(request, cancellationToken);
        }
    }
}

﻿using FluentResults;
using MediatR;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public class MediatorCommandBus : ICommandBus
    {
        private readonly IMediator _mediator;

        public MediatorCommandBus(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Result> Execute(ICommand command)
        {
            return await _mediator.Send(command);
        }

        public async Task<Result<TResult>> Execute<TResult>(ICommand<TResult> command)
        {
            return await _mediator.Send(command);
        }
    }
}

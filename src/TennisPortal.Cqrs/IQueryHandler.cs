﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public interface IQueryHandler<TQuery, TResult> : IRequestHandler<TQuery, TResult>
        where TQuery : IQuery<TResult>
    {
        new Task<TResult> Handle(TQuery query, CancellationToken cancellationToken);

        async Task<TResult> IRequestHandler<TQuery, TResult>.Handle(
            TQuery request,
            CancellationToken cancellationToken)
        {
            return await Handle(request, cancellationToken);
        }
    }
}

﻿using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public class MediatorEventDispatcher : IEventDispatcher
    {
        private readonly IMediator _mediator;

        public MediatorEventDispatcher(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Dispatch(IReadOnlyCollection<IDomainEvent> events)
        {
            foreach (var @event in events)
            {
                await _mediator.Publish(@event);
            }
        }
    }
}

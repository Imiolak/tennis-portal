﻿using FluentResults;
using MediatR;

namespace TennisPortal.Cqrs
{
    public interface ICommand : IRequest<Result>
    {
    }

    public interface ICommand<TResult> : IRequest<Result<TResult>>
    {
    }
}

﻿using MediatR;
using System.Threading.Tasks;

namespace TennisPortal.Cqrs
{
    public class MediatorQueryBus : IQueryBus
    {
        private readonly IMediator _mediator;

        public MediatorQueryBus(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<TResult> Execute<TResult>(IQuery<TResult> query)
        {
            return await _mediator.Send(query);
        }
    }
}
